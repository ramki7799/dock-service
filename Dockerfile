FROM openjdk:11
EXPOSE 8183
ARG JAR_FILE=target/fwd-inv-dock-service-1.0-SNAPSHOT.jar
COPY ${JAR_FILE} fwd-inv-dock-service.jar
ENTRYPOINT ["java","-jar","/fwd-inv-dock-service.jar"]

