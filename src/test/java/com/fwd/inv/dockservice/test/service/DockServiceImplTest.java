package com.fwd.inv.dockservice.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import com.fwd.inv.dockservice.model.Dock;
import com.fwd.inv.dockservice.repository.DockRepository;
import com.fwd.inv.dockservice.service.DockServiceImpl;
import com.fwd.inv.dockservice.util.CommonUtils;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DockServiceImplTest {

	@InjectMocks private DockServiceImpl dockService;

	private List<Dock> dockList=new ArrayList<>();

	  @Mock private DockRepository repository;

	  LocalDateTime openDockTime=CommonUtils.getTodayDockOpenTime();

	  LocalDateTime closeDockTime=CommonUtils.getTodayDockCloseTime();


	  @BeforeEach
	  void setUp() throws Exception{
		  dockService=new DockServiceImpl();
	  }

	  @Test
		public void it_should_be_return_AllDocksByLocation_test(){
			Dock e=new Dock("345671","PLANO", "DRYVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			dockList.add(e);
			dockList.add(d);
			when(repository.findAllByLocationId(Mockito.anyString())).thenReturn(dockList);

			List<Dock> docks=dockService.getDocksByLocation("TEXAS");

			assertNotNull(docks);
			assertEquals(2,docks.size());
			assertEquals("PLANO",docks.get(0).getFacilityId());

	  }

	  @Test(expected = Exception.class)
		public void it_should_throw_Exception_AllDocksByLocation_test() throws Exception{
			when(repository.findAllByLocationId(Mockito.anyString())).thenThrow(new RuntimeException());
			List<Dock> docks=dockService.getDocksByLocation(Mockito.anyString());
	        assertNull(docks);
	  }
	  @Test
	  public void it_should_be_return_AllDocksByLocationAndFacility_test(){
		  	Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			dockList.add(d);
			when(repository.findAllByLocationIdAndFacilityId(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
			List<Dock> docks=dockService.getDocksByLocationAndFacility("TEXAS", "PLANO");
			assertEquals(1,docks.size());
			assertEquals("TEXAS",docks.get(0).getLocationId());
	  }
	  @Test(expected = Exception.class)
	  public void it_should_throw_Exception_AllDocksByLocationAndFacility_test(){
			when(repository.findAllByLocationIdAndFacilityId(Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
			List<Dock> docks=dockService.getDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString());
			 assertNull(docks);
	  }
	  @Test
	  public void it_should_be_return_AllDocksByLocationAndFacilityAndDockType_test(){
		  	Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
		  	dockList.add(d);
			when(repository.findAllByLocationIdAndFacilityIdAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
			List<Dock> docks=dockService.getAllDocksByLocationAndFacilityAndDockType("TEXAS", "PLANO","BOX");
			assertEquals(1,docks.size());
			assertEquals("TEXAS",docks.get(0).getLocationId());
	  }
	  @Test(expected = Exception.class)
	  public void it_should_throw_Exception_AllDocksByLocationAndFacilityAndDockType_test(){
			when(repository.findAllByLocationIdAndFacilityIdAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
			List<Dock> docks=dockService.getAllDocksByLocationAndFacilityAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
			 assertNull(docks);
	  }
	  @Test
	  public void it_should_be_return_AllDocksByFacilityAndDockTypeAndDockStatus_test(){
		  	Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			dockList.add(d);
			when(repository.findAllByFacilityIdAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
			List<Dock> docks=dockService.getAllDocksByFacilityAndDockTypeAndDockStatus("PLANO","BOX","AVAILABLE");
			assertEquals(1,docks.size());
			assertEquals("TEXAS",docks.get(0).getLocationId());
	  }
	  @Test(expected = Exception.class)
	  public void it_should_throw_Exception_AllDocksByFacilityAndDockTypeAndDockStatus_test(){
			when(repository.findAllByFacilityIdAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
			List<Dock> docks=dockService.getAllDocksByFacilityAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
			 assertNull(docks);
	  }
	  @Test
	  public void it_should_return_DocksByFacility_test() {
		  Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			dockList.add(d);
			when(repository.findByFacilityIdAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
			List<Dock> docks=dockService.getDocksByfacilityAndDockStatus("PLANO","AVAILABLE");
			assertEquals(1,docks.size());
			assertEquals("TEXAS",docks.get(0).getLocationId());

	  }
	  @Test(expected = Exception.class)
	  public void it_should_throw_Exception_DocksByFacility_test() {
			when(repository.findByFacilityIdAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
			List<Dock> docks=dockService.getDocksByfacilityAndDockStatus(Mockito.anyString(),Mockito.anyString());
			assertNull(docks);

	  }
	  @Test
	  public void it_should_return_DockById_test() {
		  Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
			when(repository.findByDockId(Mockito.anyString())).thenReturn(d);
			Dock dock=dockService.getByDockId("345672");
			assertNotNull(dock);
			assertEquals("TEXAS",dock.getLocationId());
	  }
	  @Test(expected = Exception.class)
	  public void it_should_throw_Exception_DockById_test() {
			when(repository.findByDockId(Mockito.anyString())).thenThrow(new RuntimeException());
			Dock dock=dockService.getByDockId(Mockito.anyString());
			assertNull(dock);
	  }


      @Test
      public void it_should_createDock_test() {
    	  Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
    	  when(repository.save(Mockito.any(Dock.class))).thenReturn(d);
    	  Dock dock=dockService.createDock(d);
    	  assertNotNull(dock);
		  assertEquals("TEXAS",dock.getLocationId());

      }
      @Test(expected = Exception.class)
      public void it_should_throw_Exception_createDock_test() {
    	  when(repository.save(Mockito.any(Dock.class))).thenThrow(new Exception());
    	  Dock k=dockService.createDock(Mockito.any(Dock.class));
    	 assertNull(k);

      }

      @Test
      public void it_should_deleteById_test() {
    	  doAnswer(new Answer<ResponseEntity<HttpStatus>>() {
    		    @Override
				public ResponseEntity<HttpStatus> answer(InvocationOnMock invocation) {
    		      return new ResponseEntity<>(HttpStatus.OK);
    		    }
    		}).when(repository).deleteById(Mockito.anyString());
    	  assertEquals(new ResponseEntity<>(HttpStatus.OK),dockService.deleteDock("987453278"));
      }
      @Test
      public void it_should_throw_Exception_deleteById_test(){
    	  doThrow(new RuntimeException()).when(repository).deleteById(Mockito.anyString());
    	  assertEquals(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR),dockService.deleteDock(Mockito.anyString()));
      }
      @Test
      public void it_should_deleteAllDocks_test() {
    	  doAnswer(new Answer<ResponseEntity<HttpStatus>>() {
    		    @Override
				public ResponseEntity<HttpStatus> answer(InvocationOnMock invocation) {
    		      return new ResponseEntity<>(HttpStatus.OK);
    		    }
    		}).when(repository).deleteAll();
    	  assertEquals(new ResponseEntity<>(HttpStatus.OK),dockService.deleteAllDocks());
      }
      @Test
      public void it_should_throw_Exception_deleteAllDocks_test() {
    	  doThrow(new RuntimeException()).when(repository).deleteAll();
    	  assertEquals(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR),dockService.deleteAllDocks());
      }

}
