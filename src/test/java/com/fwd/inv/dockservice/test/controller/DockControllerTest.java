package com.fwd.inv.dockservice.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import com.fwd.inv.dockservice.controller.DockController;
import com.fwd.inv.dockservice.dto.DockDto;
import com.fwd.inv.dockservice.model.Dock;
import com.fwd.inv.dockservice.service.DockService;
import com.fwd.inv.dockservice.util.CommonUtils;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
  public class DockControllerTest {

   @InjectMocks DockController controller;

   @Mock private DockService service;

   LocalDateTime openDockTime=CommonUtils.getTodayDockOpenTime();

   LocalDateTime closeDockTime=CommonUtils.getTodayDockCloseTime();

   LocalDateTime dockAssignTime=LocalDateTime.now().with(LocalTime.of(14, 0, 0));

   LocalDateTime dockActualReleaseTime=LocalDateTime.now().with(LocalTime.of(14, 10, 0));

  private List<Dock> dockList=new ArrayList<>();


  @BeforeEach
  void setUp() throws Exception{
	  controller=new DockController();
   }


  @Test
  public void it_should_be_return_AllDocksByLocation_test() throws Exception {
	    Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	    Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
		dockList.add(e);
		dockList.add(d);
       when(service.getDocksByLocation(Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocation("TEXAS");
        assertNotNull(docks.getBody());
		assertEquals("TEXAS",docks.getBody().get(0).getLocationId());
		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_Null_AllDocksByLocation_test() throws Exception {
       when(service.getDocksByLocation(Mockito.anyString())).thenReturn(null);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocation(Mockito.anyString());
        assertNull(docks.getBody());
   		assertEquals(HttpStatus.BAD_REQUEST,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_EmptyList_AllDocksByLocation_test() throws Exception {
       when(service.getDocksByLocation(Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocation("TEXAS");
        assert(docks.getBody().isEmpty());
   		assertEquals(HttpStatus.NO_CONTENT,docks.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_AllDocksByLocation_test() throws Exception {
       when(service.getDocksByLocation(Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocation("TEXAS");
        assertNull(docks.getBody());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_AllDocksByLocationAndFacility_test() throws Exception {
	   Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
		dockList.add(e);
       when(service.getDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacility("TEXAS","PLANO");
        assertNotNull(docks.getBody());
		assertEquals("PLANO",docks.getBody().get(0).getFacilityId());
		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_Null_AllDocksByLocationAndFacility_test() throws Exception {
       when(service.getDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString())).thenReturn(null);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString());
       assertNull(docks.getBody());
       assertEquals(HttpStatus.BAD_REQUEST,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_EmptyList_AllDocksByLocationAndFacility_test() throws Exception {
       when(service.getDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacility("TEXAS","plano");
        assert(docks.getBody().isEmpty());
  		assertEquals(HttpStatus.NO_CONTENT,docks.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_AllDocksByLocationAndFacility_test() throws Exception {
       when(service.getDocksByLocationAndFacility(Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacility("TEXAS","plano");
        assertNull(docks.getBody());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_AllDocksByLocationAndFacilityAndType_test() throws Exception {
	  Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
		dockList.add(e);
       when(service.getAllDocksByLocationAndFacilityAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacilityAndType("TEXAS","plano","BOX");
        assertNotNull(docks.getBody());
		assertEquals("PLANO",docks.getBody().get(0).getFacilityId());
		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_Null_AllDocksByLocationAndFacilityAndType_test() throws Exception {
       when(service.getAllDocksByLocationAndFacilityAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(null);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacilityAndType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
       assertNull(docks.getBody());
       assertEquals(HttpStatus.BAD_REQUEST,docks.getStatusCode());
  }
  @Test
  public void it_should_be_return_EmptyList_AllDocksByLocationAndFacilityAndType_test() throws Exception {
       when(service.getAllDocksByLocationAndFacilityAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacilityAndType("TEXAS","plano","Trailer");
        assert(docks.getBody().isEmpty());
  		assertEquals(HttpStatus.NO_CONTENT,docks.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_AllDocksByLocationAndFacilityAndType_test() throws Exception {
       when(service.getAllDocksByLocationAndFacilityAndDockType(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByLocationAndFacilityAndType("TEXAS","plano","Trailer");
        assertNull(docks.getBody());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,docks.getStatusCode());
  }

  @Test
  public void it_should_be_return_AllDocksByFacilityAndTypeAndStatus_test() throws Exception {
	  Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
		dockList.add(e);
       when(service.getAllDocksByFacilityAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByFacilityAndTypeAndStatus("plano","BOX","Available");
        assertNotNull(docks.getBody());
		assertEquals("PLANO",docks.getBody().get(0).getFacilityId());
		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_return_Null_AllDocksByFacilityAndTypeAndStatus_test() throws Exception {
       when(service.getAllDocksByFacilityAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(null);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByFacilityAndTypeAndStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString());
        assertNull(docks.getBody());
		assertEquals(HttpStatus.BAD_REQUEST,docks.getStatusCode());
  }

  @Test
  public void it_should_be_return_EmptyList_AllDocksByFacilityAndTypeAndStatus_test() throws Exception {
       when(service.getAllDocksByFacilityAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByFacilityAndTypeAndStatus("plano","BOX","Available");
        assert(docks.getBody().isEmpty());
  		assertEquals(HttpStatus.NO_CONTENT,docks.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_AllDocksByFacilityAndTypeAndStatus_test() throws Exception {
       when(service.getAllDocksByFacilityAndDockTypeAndDockStatus(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<List<Dock>> docks=controller.getAllDocksByFacilityAndTypeAndStatus("plano","BOX","Available");
        assertNull(docks.getBody());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,docks.getStatusCode());
  }

  @Test
  public void it_should_return_DockByDockId_test() throws Exception {
	  Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
       when(service.getByDockId(Mockito.anyString())).thenReturn(e);
       ResponseEntity<Dock> docks=controller.getDockByDockId("345672");
        assertNotNull(docks.getBody());
		assertEquals("345672",docks.getBody().getDockId());
		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_return_null_DockByDockId_test() throws Exception {
       when(service.getByDockId(Mockito.anyString())).thenReturn(null);
       ResponseEntity<Dock> docks=controller.getDockByDockId("23456");
        assertNull(docks.getBody());
		assertEquals(HttpStatus.NO_CONTENT,docks.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_DockByDockId_test() throws Exception {
       when(service.getByDockId(Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<Dock> docks=controller.getDockByDockId("23456");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,docks.getStatusCode());
  }
  @Test
  public void it_should_return_DocksByFacility_test() throws Exception {
	  Dock e=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  dockList.add(e);
       when(service.getDocksByfacilityAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> docks=controller.getAvailableDocksByFacilityId("PLANO");
  		assertEquals("PLANO",docks.getBody().get(0).getFacilityId());
  		assertEquals(HttpStatus.OK,docks.getStatusCode());
  }
  @Test
  public void it_should_return_EmptyList_DocksByFacility_test() throws Exception {
       when(service.getDocksByfacilityAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenReturn(dockList);
       ResponseEntity<List<Dock>> dock=controller.getAvailableDocksByFacilityId("PLANO");
       assert(dock.getBody().isEmpty());
  	   assertEquals(HttpStatus.NO_CONTENT,dock.getStatusCode());
  }
  @Test
  public void it_should_return_Null_DocksByFacility_test() throws Exception {
       when(service.getDocksByfacilityAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenReturn(null);
       ResponseEntity<List<Dock>> dock=controller.getAvailableDocksByFacilityId(Mockito.anyString());
       assertNull(dock.getBody());
  	   assertEquals(HttpStatus.BAD_REQUEST,dock.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_DocksByFacility_test() throws Exception {
       when(service.getDocksByfacilityAndDockStatus(Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
       ResponseEntity<List<Dock>> dock=controller.getAvailableDocksByFacilityId("PLANO");
       assertNull(dock.getBody());
  	   assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,dock.getStatusCode());
  }
  @Test
  public void it_should_return_createdDock_test() {
	  Dock d=new Dock("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  DockDto e=new DockDto("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  when(service.createDock(Mockito.any(Dock.class))).thenReturn(d);
	  ResponseEntity<Dock> dock=controller.createDock(e);
	  assertNotNull(dock.getBody());
	  assertEquals(HttpStatus.OK,dock.getStatusCode());

  }
  @Test
  public void it_should_return_null_createdDock_test() {
	  DockDto e=new DockDto("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  Mockito.lenient().when(service.createDock(Mockito.any(Dock.class))).thenReturn(null);
	  ResponseEntity<Dock> d=controller.createDock(e);
	  assertNull(d.getBody());
	  assertEquals(HttpStatus.NO_CONTENT,d.getStatusCode());
  }
  @Test
  public void it_should_return_Null_createDock_test() {
	  DockDto e=new DockDto("3456^72","PLANO*", "BOX ", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  Mockito.lenient().when(service.createDock(Mockito.any(Dock.class))).thenReturn(null);
	  ResponseEntity<Dock> d=controller.createDock(e);
	  assertNull(d.getBody());
	  assertEquals(HttpStatus.BAD_REQUEST,d.getStatusCode());
  }
  @Test
  public void it_should_throw_Exception_createDock_test() {
	  DockDto e=new DockDto("345672","PLANO", "BOX", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,null,null,null,0);
	  Mockito.lenient().when(service.createDock(Mockito.any(Dock.class))).thenThrow(new RuntimeException());
	  ResponseEntity<Dock> d=controller.createDock(e);
	  assertNull(d.getBody());
	  assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,d.getStatusCode());
  }
  @Test
  public void it_should_return_updatedDock_test() {
	  Dock e=new Dock("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  DockDto dto=new DockDto("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  when(service.getByDockId(Mockito.anyString())).thenReturn(e);
	  when(service.createDock(Mockito.any(Dock.class))).thenReturn(e);
	  ResponseEntity<Dock> dock=controller.updateDockById("345672",dto);
	  assertNotNull(dock.getBody());
	  assertEquals(HttpStatus.OK,dock.getStatusCode());

  }
  @Test
  public void it_should_return_EmptyRecord_updatedDock_test() {
	  DockDto dto=new DockDto("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  when(service.getByDockId(Mockito.anyString())).thenReturn(null);
	  ResponseEntity<Dock> dock=controller.updateDockById("1",dto);
	  assertNull(dock.getBody());
	  assertEquals(HttpStatus.NO_CONTENT,dock.getStatusCode());

  }
  @Test
  public void it_should_throw_Exception_updatedDock_test(){
	  DockDto dto=new DockDto("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  Dock e=new Dock("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  when(service.getByDockId(Mockito.anyString())).thenReturn(e);
	  when(service.createDock(Mockito.any(Dock.class))).thenThrow(new RuntimeException());
	  ResponseEntity<Dock> dock=controller.updateDockById("1",dto);
	  assertNull(dock.getBody());
	  assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,dock.getStatusCode());

  }
  @Test
  public void it_should_return_null_updatedDock_test(){
	  DockDto dto=new DockDto("345672","PLAN^O", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,LocalDateTime.now(),LocalDateTime.now().plusMinutes(10),LocalDateTime.now().plusMinutes(10),10);
	  Dock e=new Dock("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,LocalDateTime.now(),LocalDateTime.now().plusMinutes(10),LocalDateTime.now().plusMinutes(10),10);
	  when(service.getByDockId(Mockito.anyString())).thenReturn(e);
	  when(service.createDock(Mockito.any(Dock.class))).thenThrow(new RuntimeException());
	  ResponseEntity<Dock> dock=controller.updateDockById("1",dto);
	  assertNull(dock.getBody());
	  assertEquals(HttpStatus.BAD_REQUEST,dock.getStatusCode());

  }

  @Test
  public void it_should_throw_Exception_Retrieval_updateDock_test() {
	  DockDto dto=new DockDto("345672","PLANO", "BOXVAN", "AVAILABLE", "TEXAS",openDockTime,closeDockTime,dockAssignTime,dockActualReleaseTime,dockActualReleaseTime,10);
	  when(service.getByDockId(Mockito.anyString())).thenThrow(new RuntimeException());
	  ResponseEntity<Dock> dock=controller.updateDockById("1",dto);
	  assertNull(dock.getBody());
	  assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,dock.getStatusCode());

  }
  @Test
  public void it_should_deleteDockById_test() {
	  doAnswer(new Answer<ResponseEntity<HttpStatus>>() {
		    @Override
			public ResponseEntity<HttpStatus> answer(InvocationOnMock invocation) {
		      return new ResponseEntity<>(HttpStatus.OK);
		    }
		}).when(service).deleteDock(Mockito.anyString());
	  assertEquals(new ResponseEntity<>(HttpStatus.OK),controller.deleteDockById("987453278"));
  }
  @Test
  public void it_should_throw_Exception_deleteDockById_test() {
	  doThrow(new RuntimeException()).when(service).deleteDock(Mockito.anyString());
	  assertEquals(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR),controller.deleteDockById("1"));
  }
  @Test
  public void it_should_deleteAllDocks_test() {
	  doAnswer(new Answer<ResponseEntity<HttpStatus>>() {
		    @Override
			public ResponseEntity<HttpStatus> answer(InvocationOnMock invocation) {
		      return new ResponseEntity<>(HttpStatus.OK);
		    }
		}).when(service).deleteAllDocks();
	  assertEquals(new ResponseEntity<>(HttpStatus.OK),controller.deleteAllDocks());
  }
  @Test
  public void it_should_throw_Exception_deleteAllDocks_test() {
	  doThrow(new RuntimeException()).when(service).deleteAllDocks();
	  assertEquals(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR),controller.deleteAllDocks());
  }

  }
