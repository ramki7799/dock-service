package com.fwd.inv.dockservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockServiceApplication {


	private static final Logger LOGGER = LogManager.getLogger(DockServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DockServiceApplication.class, args);
		LOGGER.info("DockServiceApplication started...");

	}

}
