package com.fwd.inv.dockservice.repository;

import com.fwd.inv.dockservice.model.Dock;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author SA20184845
 *
 */

public interface DockRepository extends CassandraRepository<Dock,String> {

	@Query(value="${Dock.getDocksByLocationIdAndFacilityIdAndType}")
	List<Dock> finddocksBylocationIdAndFacilityIdAndType(@Param("locationId") String locationId,@Param("facilityId") String facilityId,
			@Param("type") String type);


	//@Query(value="SELECT * FROM T_DOCK WHERE FACILITY_ID= :facilityId AND DOCK_STATUS= :dockStatus  AND DOCK_ACT_REL_TIME > currentZonedDateTime allow filtering")
	@AllowFiltering
	List<Dock> findByFacilityIdAndDockStatus(String facilityId,String dockStatus);


	@AllowFiltering
	List<Dock> findAllByLocationIdAndFacilityId(String locationId,String facilityId);

	@AllowFiltering
	List<Dock> findAllByLocationIdAndFacilityIdAndDockType(String locationId,String facilityId,String dockType);

	@AllowFiltering
	List<Dock> findAllByFacilityIdAndDockTypeAndDockStatus(String facilityId,String dockType,String dockStatus);

	@AllowFiltering
	List<Dock> findAllByLocationId(String locationId);

	@AllowFiltering
	Dock findByDockId(String dockNo);

	List<Dock> findAllByFacilityIdAndDockStatus(String facilityId,String dockStatus);

	@Override
	Dock save(Dock dock);

	@Override
	@Transactional
	void deleteById(String id);

	@Override
	@Transactional
	void deleteAll();


}
