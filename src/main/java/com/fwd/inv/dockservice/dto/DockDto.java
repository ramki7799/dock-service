 package com.fwd.inv.dockservice.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class DockDto  extends BaseDto implements Serializable{


	private static final long serialVersionUID = -8537763392284606035L;

	private String dockId;

	private String facilityId;

	private String dockType;

	private String dockStatus;

	private String locationId;

	private LocalDateTime dockOpenTime;

	private LocalDateTime dockCloseTime;

	private LocalDateTime dockAssignTime;

	private LocalDateTime dockExpectedReleaseTime;

	private LocalDateTime dockActualReleaseTime;

	private int dockTimeInterval;



	public DockDto() {
		super();
	}



	public DockDto(String dockId, String facilityId, String dockType, String dockStatus, String locationId, LocalDateTime dockOpenTime,
			LocalDateTime dockCloseTime, LocalDateTime dockAssignTime, LocalDateTime dockExpectedReleaseTime, LocalDateTime dockActualReleaseTime,
			int dockTimeInterval) {
		super();
		this.dockId = dockId;
		this.facilityId = facilityId;
		this.dockType = dockType;
		this.dockStatus = dockStatus;
		this.locationId = locationId;
		this.dockOpenTime = dockOpenTime;
		this.dockCloseTime = dockCloseTime;
		this.dockAssignTime = dockAssignTime;
		this.dockExpectedReleaseTime = dockExpectedReleaseTime;
		this.dockActualReleaseTime = dockActualReleaseTime;
		this.dockTimeInterval = dockTimeInterval;
	}



	public String getDockId()
	{
		return dockId;
	}



	public void setDockId(String dockId)
	{
		this.dockId = dockId;
	}



	public String getFacilityId()
	{
		return facilityId;
	}



	public void setFacilityId(String facilityId)
	{
		this.facilityId = facilityId;
	}



	public String getDockType()
	{
		return dockType;
	}



	public void setDockType(String dockType)
	{
		this.dockType = dockType;
	}



	public String getDockStatus()
	{
		return dockStatus;
	}



	public void setDockStatus(String dockStatus)
	{
		this.dockStatus = dockStatus;
	}



	public String getLocationId()
	{
		return locationId;
	}



	public void setLocationId(String locationId)
	{
		this.locationId = locationId;
	}



	public LocalDateTime getDockOpenTime()
	{
		return dockOpenTime;
	}



	public void setDockOpenTime(LocalDateTime dockOpenTime)
	{
		this.dockOpenTime = dockOpenTime;
	}



	public LocalDateTime getDockCloseTime()
	{
		return dockCloseTime;
	}



	public void setDockCloseTime(LocalDateTime dockCloseTime)
	{
		this.dockCloseTime = dockCloseTime;
	}



	public LocalDateTime getDockAssignTime()
	{
		return dockAssignTime;
	}



	public void setDockAssignTime(LocalDateTime dockAssignTime)
	{
		this.dockAssignTime = dockAssignTime;
	}



	public LocalDateTime getDockExpectedReleaseTime()
	{
		return dockExpectedReleaseTime;
	}



	public void setDockExpectedReleaseTime(LocalDateTime dockExpectedReleaseTime)
	{
		this.dockExpectedReleaseTime = dockExpectedReleaseTime;
	}



	public LocalDateTime getDockActualReleaseTime()
	{
		return dockActualReleaseTime;
	}



	public void setDockActualReleaseTime(LocalDateTime dockActualReleaseTime)
	{
		this.dockActualReleaseTime = dockActualReleaseTime;
	}



	public int getDockTimeInterval()
	{
		return dockTimeInterval;
	}



	public void setDockTimeInterval(int dockTimeInterval)
	{
		this.dockTimeInterval = dockTimeInterval;
	}



	@Override
	public String toString()
	{
		return "DockDto [dockId=" + dockId + ", facilityId=" + facilityId + ", dockType=" + dockType + ", dockStatus=" + dockStatus + ", locationId="
			+ locationId + ", dockOpenTime=" + dockOpenTime + ", dockCloseTime=" + dockCloseTime + ", dockAssignTime=" + dockAssignTime
			+ ", dockExpectedReleaseTime=" + dockExpectedReleaseTime + ", dockActualReleaseTime=" + dockActualReleaseTime + ", dockTimeInterval="
			+ dockTimeInterval + "]";
	}

}
