 package com.fwd.inv.dockservice.dto;

import java.time.LocalDateTime;

public abstract class BaseDto {

	public String createdBy;

	public LocalDateTime createdDateTime;

	public String lastUpdatedBy;

	public LocalDateTime lastUpdatedTimeStamp;

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDateTime()
	{
		return createdDateTime;
	}

	public void setCreatedDateTime(LocalDateTime createdDateTime)
	{
		this.createdDateTime = createdDateTime;
	}

	public String getLastUpdatedBy()
	{
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy)
	{
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public LocalDateTime getLastUpdatedTimeStamp()
	{
		return lastUpdatedTimeStamp;
	}

	public void setLastUpdatedTimeStamp(LocalDateTime lastUpdatedTimeStamp)
	{
		this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
	}



}
