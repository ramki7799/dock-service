 package com.fwd.inv.dockservice.util;

import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public  class CommonUtils {


	public CommonUtils() {
		super();
	}

	public static LocalDate convertStringToDate(String str) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	    if(!StringUtils.isEmpty(str)) {
			return LocalDate.parse(str, formatter);
		} else {
			return LocalDate.now();
		}
	}

	public static LocalDateTime convertStringToTimeStamp(String str) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

		if(!StringUtils.isEmpty(str) && str.contains(Character.toString('T'))) {
			return LocalDateTime.parse(str, formatter);
		} else {
			return LocalDateTime.now().withNano(0);
		}
	}
	public static LocalDateTime getDockAssignTime() {
		return LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
    }

	public static String convertDateToString(String date) {
		return LocalDate.parse(date).toString();
	}
	public static String convertTimeStampToString(String timeStamp) {
		return LocalDateTime.parse(timeStamp).toString();
	}

	public static  LocalDateTime getTodayDockOpenTime() {
		return LocalDateTime.now().with(LocalTime.of(9, 0, 0));
	}
	public static  LocalDateTime getTodayDockCloseTime() {
		return LocalDateTime.now().with(LocalTime.of(16, 0, 0));
	}
	public static LocalDateTime convertStringToZoneDateTime(String str) {
		return LocalDateTime.parse(str,DateTimeFormatter.ISO_OFFSET_DATE_TIME);

	}

}
