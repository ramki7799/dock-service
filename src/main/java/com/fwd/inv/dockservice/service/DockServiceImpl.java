package com.fwd.inv.dockservice.service;

import com.fwd.inv.dockservice.model.Dock;
import com.fwd.inv.dockservice.repository.DockRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DockServiceImpl implements DockService{


	@Autowired
	DockRepository dockRepository;

	@Override
	public List<Dock> getDocksByLocation(String locationId){

		return dockRepository.findAllByLocationId(locationId);
	}
	@Override
	public List<Dock> getDocksByLocationAndFacility(String locationId,String facilityId){
		return dockRepository.findAllByLocationIdAndFacilityId(locationId, facilityId);
	}
	@Override
	public List<Dock> getAllDocksByLocationAndFacilityAndDockType(String locationId,String facilityId,String docktype){
		return dockRepository.findAllByLocationIdAndFacilityIdAndDockType(locationId, facilityId,docktype);
	}
	@Override
	public List<Dock> getAllDocksByFacilityAndDockTypeAndDockStatus(String facilityId,String docktype,String dockStatus){
		return dockRepository.findAllByFacilityIdAndDockTypeAndDockStatus(facilityId,docktype,dockStatus);
	}
	@Override
	public List<Dock> getDocksByfacilityAndDockStatus(String facilityId,String dockStatus){
		return dockRepository.findByFacilityIdAndDockStatus(facilityId,dockStatus);
	}

	@Override
	public Dock getByDockId(String dockId) {
		return dockRepository.findByDockId(dockId);
	}
	@Override
	public Dock createDock(Dock dock) {
		return dockRepository.save(dock);
	}

	@Override
	public ResponseEntity<HttpStatus> deleteDock(String id){
		try {
			dockRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);

		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	@Override
	public ResponseEntity<HttpStatus> deleteAllDocks(){
		try {
			dockRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.OK);

		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
