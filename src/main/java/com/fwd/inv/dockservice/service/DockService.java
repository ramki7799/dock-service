package com.fwd.inv.dockservice.service;

import com.fwd.inv.dockservice.model.Dock;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DockService {

	public List<Dock> getDocksByLocation(String locationId);

	public List<Dock> getDocksByLocationAndFacility(String locationId,String facilityId);

	public List<Dock> getAllDocksByLocationAndFacilityAndDockType(String locationId,String facilityId,String docktype);

	public List<Dock> getAllDocksByFacilityAndDockTypeAndDockStatus(String facilityId,String docktype, String dockStatus);

	public List<Dock> getDocksByfacilityAndDockStatus(String facilityId,String dockStatus);

	public Dock getByDockId(String dockId);

	public Dock createDock(Dock dock);

	public ResponseEntity<HttpStatus> deleteDock(String id);

	public ResponseEntity<HttpStatus> deleteAllDocks();

}
