 package com.fwd.inv.dockservice.constants;

 public class DockServiceConstants {

	 public static final  String URI_DOCKSBYLOCATIONID="/api/getAllDocksByLocation/{locationId}";

	 public static final String URI_DOCKSBYLOCATIONANDFACILITY="/api/getAllDocksByLocationAndFacility/{locationId}/{facilityId}";

	 public static final String URI_DOCKSBYLOCATIONANDFACILITYANDTYPE="/api/getAllDocksByLocationAndFacilityAndType/{locationId}/{facilityId}/{dockType}";

	 public static final String URI_DOCKSBYFACILITYANDTYPEANDSTATUS="/api/getAllDocksByFacilityAndTypeAndStatus/{facilityId}/{dockType}/{dockStatus}";

	 public static final String URI_DOCKBYDOCKID="/api/getDockByDockId/{dockId}";

	 public static final String URI_DOCKSBYFACILITYID="/api/getAvailableDocksByFacilityId/{facilityId}";

	 public static final String URI_CREATEDOCK="/api/createDock";

	 public static final String URI_UPDATEDOCKBYID="/api/updateDockById/{dockId}";

	 public static final String URI_DELETEDOCKBYID="/api/deleteDockById/{dockId}";

	 public static final String URI_DELETEALLDOCKS="/api/deleteAllDocks";


}
