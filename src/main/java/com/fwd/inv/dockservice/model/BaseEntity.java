 package com.fwd.inv.dockservice.model;

import org.springframework.data.cassandra.core.mapping.Column;

import java.time.LocalDateTime;

public class BaseEntity {


	@Column("CREATED_BY")
	public String createdBy;

	@Column("CREATED_DATETIME")
	public LocalDateTime createdDateTime;

	@Column("LAST_UPDATEDBY")
	public String lastUpdatedBy;

	@Column("LAST_UPDATED_TIMESTAMP")
	public LocalDateTime lastUpdatedTimeStamp;


	public BaseEntity() {
		super();
	}


	public String getCreatedBy()
	{
		return createdBy;
	}


	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}


	public LocalDateTime getCreatedDateTime()
	{
		return createdDateTime;
	}


	public void setCreatedDateTime(LocalDateTime createdDateTime)
	{
		this.createdDateTime = createdDateTime;
	}


	public String getLastUpdatedBy()
	{
		return lastUpdatedBy;
	}


	public void setLastUpdatedBy(String lastUpdatedBy)
	{
		this.lastUpdatedBy = lastUpdatedBy;
	}


	public LocalDateTime getLastUpdatedTimeStamp()
	{
		return lastUpdatedTimeStamp;
	}


	public void setLastUpdatedTimeStamp(LocalDateTime lastUpdatedTimeStamp)
	{
		this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
	}



}
