package com.fwd.inv.dockservice.model;


import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;


@Table("T_DOCK")
public class Dock extends BaseEntity {

	@PrimaryKey("DOCK_ID")
	private String dockId;

	@Column("FACILITY_ID")
	private String facilityId;

	@Column("DOCK_TYPE")
	private String dockType;

	@Column("DOCK_STATUS")
	private String dockStatus;

	@Column("LOCATION_ID")
	private String locationId;

	@Column("DOCK_OPEN_TIME")
	private LocalDateTime dockOpenTime;

	@Column("DOCK_CLOSE_TIME")
	private LocalDateTime dockCloseTime;

	@Column("DOCK_ASSIGN_TIME")
	private LocalDateTime dockAssignTime;

	@Column("DOCK_EXPT_REL_TIME")
	private LocalDateTime dockExpectedReleaseTime;

	@Column("DOCK_ACT_REL_TIME")
	private LocalDateTime dockActualReleaseTime;

	@Column("DOCK_TIME_INTVAL")
	private int dockTimeInterval;


	public Dock() {
		super();
	}


	public Dock(String dockId, String facilityId, String dockType, String dockStatus, String locationId, LocalDateTime dockOpenTime,
			LocalDateTime dockCloseTime, LocalDateTime dockAssignTime, LocalDateTime dockExpectedReleaseTime, LocalDateTime dockActualReleaseTime,
			int dockTimeInterval) {
		super();
		this.dockId = dockId;
		this.facilityId = facilityId;
		this.dockType = dockType;
		this.dockStatus = dockStatus;
		this.locationId = locationId;
		this.dockOpenTime = dockOpenTime;
		this.dockCloseTime = dockCloseTime;
		this.dockAssignTime = dockAssignTime;
		this.dockExpectedReleaseTime = dockExpectedReleaseTime;
		this.dockActualReleaseTime = dockActualReleaseTime;
		this.dockTimeInterval = dockTimeInterval;
	}


	public String getDockId()
	{
		return dockId;
	}


	public void setDockId(String dockId)
	{
		this.dockId = dockId;
	}


	public String getFacilityId()
	{
		return facilityId;
	}


	public void setFacilityId(String facilityId)
	{
		this.facilityId = facilityId;
	}


	public String getDockType()
	{
		return dockType;
	}


	public void setDockType(String dockType)
	{
		this.dockType = dockType;
	}


	public String getDockStatus()
	{
		return dockStatus;
	}


	public void setDockStatus(String dockStatus)
	{
		this.dockStatus = dockStatus;
	}


	public String getLocationId()
	{
		return locationId;
	}


	public void setLocationId(String locationId)
	{
		this.locationId = locationId;
	}


	public LocalDateTime getDockOpenTime()
	{
		return dockOpenTime;
	}


	public void setDockOpenTime(LocalDateTime dockOpenTime)
	{
		this.dockOpenTime = dockOpenTime;
	}


	public LocalDateTime getDockCloseTime()
	{
		return dockCloseTime;
	}


	public void setDockCloseTime(LocalDateTime dockCloseTime)
	{
		this.dockCloseTime = dockCloseTime;
	}


	public LocalDateTime getDockAssignTime()
	{
		return dockAssignTime;
	}


	public void setDockAssignTime(LocalDateTime dockAssignTime)
	{
		this.dockAssignTime = dockAssignTime;
	}


	public LocalDateTime getDockExpectedReleaseTime()
	{
		return dockExpectedReleaseTime;
	}


	public void setDockExpectedReleaseTime(LocalDateTime dockExpectedReleaseTime)
	{
		this.dockExpectedReleaseTime = dockExpectedReleaseTime;
	}


	public LocalDateTime getDockActualReleaseTime()
	{
		return dockActualReleaseTime;
	}


	public void setDockActualReleaseTime(LocalDateTime dockActualReleaseTime)
	{
		this.dockActualReleaseTime = dockActualReleaseTime;
	}


	public int getDockTimeInterval()
	{
		return dockTimeInterval;
	}


	public void setDockTimeInterval(int dockTimeInterval)
	{
		this.dockTimeInterval = dockTimeInterval;
	}


	@Override
	public String toString()
	{
		return "Dock [dockId=" + dockId + ", facilityId=" + facilityId + ", dockType=" + dockType + ", dockStatus=" + dockStatus + ", locationId=" + locationId
			+ ", dockOpenTime=" + dockOpenTime + ", dockCloseTime=" + dockCloseTime + ", dockAssignTime=" + dockAssignTime + ", dockExpectedReleaseTime="
			+ dockExpectedReleaseTime + ", dockActualReleaseTime=" + dockActualReleaseTime + ", dockTimeInterval=" + dockTimeInterval + "]";
	}


}
