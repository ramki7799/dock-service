package com.fwd.inv.dockservice.controller;

import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_CREATEDOCK;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DELETEALLDOCKS;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DELETEDOCKBYID;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKBYDOCKID;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKSBYFACILITYANDTYPEANDSTATUS;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKSBYFACILITYID;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKSBYLOCATIONANDFACILITY;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKSBYLOCATIONANDFACILITYANDTYPE;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_DOCKSBYLOCATIONID;
import static com.fwd.inv.dockservice.constants.DockServiceConstants.URI_UPDATEDOCKBYID;

import com.fwd.inv.dockservice.dto.DockDto;
import com.fwd.inv.dockservice.model.Dock;
import com.fwd.inv.dockservice.repository.DockRepository;
import com.fwd.inv.dockservice.service.DockService;
import com.fwd.inv.dockservice.util.CommonUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@RestController
@EnableCassandraRepositories(basePackageClasses= DockRepository.class)
@RequestMapping("/api/dock/v1")
public class DockController {


	@Autowired
	DockService dockService;

	String regex = "^[a-zA-Z0-9]+$";
	String textRegex = "^[a-zA-Z]+$";
	String digitRegex="\\d+";

	Pattern dockIdPattern = Pattern.compile(regex);
	Pattern textPattern = Pattern.compile(textRegex);
	Pattern digitPattern=Pattern.compile(digitRegex);


	@GetMapping(value=URI_DOCKSBYLOCATIONID,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<Dock>> getAllDocksByLocation(@PathVariable("locationId") String locationId){
		if((!StringUtils.isEmpty(locationId)) && textPattern.matcher(locationId).matches()) {
			try {
				List<Dock> docks= dockService.getDocksByLocation(locationId.trim().toUpperCase());
				if(!docks.isEmpty()) {
					return new ResponseEntity<>(docks,HttpStatus.OK);
				}else {
					return new ResponseEntity<>(docks,HttpStatus.NO_CONTENT);
				}
			}catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	  }

	@GetMapping(value = URI_DOCKSBYLOCATIONANDFACILITY,
			produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<Dock>> getAllDocksByLocationAndFacility(@PathVariable(value="locationId") String locationId,
			 @PathVariable(value="facilityId") String facilityId) {
		if((!StringUtils.isEmpty(locationId)) && (!StringUtils.isEmpty(facilityId)) && textPattern.matcher(locationId).matches() && textPattern.matcher(facilityId).matches()) {
			 try{
					List<Dock> dockData= dockService.getDocksByLocationAndFacility(locationId.trim().toUpperCase(), facilityId.trim().toUpperCase());
					if(!dockData.isEmpty()) {
						return new ResponseEntity<>(dockData,HttpStatus.OK);
					}else {
						return new ResponseEntity<>(dockData,HttpStatus.NO_CONTENT);
					}

				}
				catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	 }
	@GetMapping(value = URI_DOCKSBYLOCATIONANDFACILITYANDTYPE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<Dock>> getAllDocksByLocationAndFacilityAndType(@PathVariable(value="locationId") String locationId,
			 @PathVariable(value="facilityId") String facilityId,@PathVariable(value="dockType") String dockType) {
		if((!StringUtils.isEmpty(locationId)) && (!StringUtils.isEmpty(facilityId)) && (!StringUtils.isEmpty(dockType))  &&
				textPattern.matcher(locationId).matches() && textPattern.matcher(facilityId).matches() && textPattern.matcher(dockType).matches()) {
			 try{
				  	List<Dock> dockData= dockService.getAllDocksByLocationAndFacilityAndDockType(locationId.trim().toUpperCase(), facilityId.trim().toUpperCase(),dockType.trim().toUpperCase());
				  	if(!dockData.isEmpty()) {
						return new ResponseEntity<>(dockData,HttpStatus.OK);
					}else {
						return new ResponseEntity<>(dockData,HttpStatus.NO_CONTENT);
					}
				}
				catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}

		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	 }
	@GetMapping(value = URI_DOCKSBYFACILITYANDTYPEANDSTATUS,
			produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<Dock>> getAllDocksByFacilityAndTypeAndStatus(@PathVariable(value="facilityId") String facilityId,
			 @PathVariable(value="dockType") String dockType,@PathVariable(value="dockStatus") String dockStatus) {
		if((!StringUtils.isEmpty(facilityId)) && (!StringUtils.isEmpty(dockType)) && (!StringUtils.isEmpty(dockStatus))
				 && textPattern.matcher(facilityId).matches() && textPattern.matcher(dockType).matches() && textPattern.matcher(dockStatus).matches()) {
			try{
			  	List<Dock> dockData= dockService.getAllDocksByFacilityAndDockTypeAndDockStatus(facilityId.trim().toUpperCase(),dockType.trim().toUpperCase(),dockStatus.trim().toUpperCase());
			  	if(!dockData.isEmpty()) {
					return new ResponseEntity<>(dockData,HttpStatus.OK);
				}else {
					return new ResponseEntity<>(dockData,HttpStatus.NO_CONTENT);
				}
			}
			catch(Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	 }

	@GetMapping(value=URI_DOCKBYDOCKID,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Dock> getDockByDockId(@PathVariable(value="dockId") String dockId) {
		if((!StringUtils.isEmpty(dockId)) && dockIdPattern.matcher(dockId.trim()).matches()) {
			try {
				 Optional<Dock> dock=Optional.ofNullable(dockService.getByDockId(dockId.trim().toUpperCase()));
				 if(dock.isPresent()) {
						return new ResponseEntity<>(dock.get(),HttpStatus.OK);
				  }else {
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				  }
			}
			catch(Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	 }
	@GetMapping(value=URI_DOCKSBYFACILITYID,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<Dock>> getAvailableDocksByFacilityId(@PathVariable("facilityId") String facilityId) {
		if(!StringUtils.isEmpty(facilityId) && textPattern.matcher(facilityId.trim()).matches()) {
			 try {
					List<Dock> dockData=dockService.getDocksByfacilityAndDockStatus(facilityId.trim().toUpperCase(),"AVAILABLE");
					 if(!dockData.isEmpty()) {
							return new ResponseEntity<>(dockData,HttpStatus.OK);
					  }else {
							return new ResponseEntity<>(dockData,HttpStatus.NO_CONTENT);
					  }
				}
				catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	 }
	 @PostMapping(value=URI_CREATEDOCK,consumes=MediaType.APPLICATION_JSON_VALUE,
			 produces=MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<Dock> createDock(@RequestBody DockDto dock) {
		 if(validateDockRequest(dock)) {
			 try {
				  Dock createDock=new Dock();
				  BeanUtils.copyProperties(dock, createDock);
				  setCommonDockFields(createDock);
				  Optional<Dock> savedDock=Optional.ofNullable(dockService.createDock(createDock));
				  if(savedDock.isPresent()) {
						return new ResponseEntity<>(savedDock.get(),HttpStatus.OK);
				  }else {
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				  }
				}
				catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		 }else {
			 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		 }



	 }


	@PutMapping(value=URI_UPDATEDOCKBYID,consumes=MediaType.APPLICATION_JSON_VALUE,
			 produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<Dock> updateDockById(@PathVariable("dockId") String id,@RequestBody DockDto dock) {
		if(validateDockRequest(dock)) {
			try {
				  Optional<Dock> dockData=Optional.ofNullable(dockService.getByDockId(id.trim().toUpperCase()));
				     if(dockData.isPresent()) {
				    	    Dock updatedDock=dockData.get();
				    	    setCommonDockFields(updatedDock);
				    	    validateAndMapDockFields(updatedDock,dock);
					    	updatedDock.setLastUpdatedBy(dock.getLastUpdatedBy());
					    	updatedDock.setDockType(dock.getDockType());
					    	updatedDock.setDockActualReleaseTime(dock.getDockActualReleaseTime());
					    	return new ResponseEntity<>(dockService.createDock(updatedDock),HttpStatus.OK);

				     } else {
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					}

				}
				catch(HttpClientErrorException ex) {
					return new ResponseEntity<>(ex.getStatusCode());
				}
				catch(Exception e) {
						return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	 }
	 private void validateAndMapDockFields(Dock updatedDock, DockDto dock)
	{
			updatedDock.setDockTimeInterval(updatedDock.getDockTimeInterval()+dock.getDockTimeInterval());
	    	updatedDock.setDockExpectedReleaseTime(dock.getDockExpectedReleaseTime());
	    	 if(null!=dock.getDockAssignTime() && dock.getDockAssignTime().isAfter(updatedDock.getDockOpenTime()) &&
	    	    		dock.getDockAssignTime().isBefore(updatedDock.getDockCloseTime())) {
	    	    	updatedDock.setDockAssignTime(dock.getDockAssignTime());
	    	    }else {
	    	    	throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
	    	    }

	    	if(null!=dock.getDockActualReleaseTime() && LocalDateTime.now().compareTo(updatedDock.getDockActualReleaseTime())>1) {
					updatedDock.setDockStatus("AVAILABLE");
	    	}
	    	else {
	    		updatedDock.setDockStatus("UNAVAILABLE");
	    	}
	}

	@DeleteMapping(value=URI_DELETEDOCKBYID,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<HttpStatus> deleteDockById(@PathVariable("dockId") String id){
		 if(dockIdPattern.matcher(id).matches()) {
			 try {
				 	return dockService.deleteDock(id.trim().toUpperCase());
			 }
			 catch(Exception e) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			 }

		 }else {
			 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		 }

	 }
	 @DeleteMapping(value=URI_DELETEALLDOCKS,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<HttpStatus> deleteAllDocks(){
		 try {
			 	return dockService.deleteAllDocks();
		 }
		 catch(Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	 }
	 private boolean validateDockRequest(DockDto dock)
		{
			boolean flag=false;
			DockDto tempDock=new DockDto();
			BeanUtils.copyProperties(dock, tempDock);
			if((!StringUtils.isEmpty(dock.getDockId())) && (!StringUtils.isEmpty(dock.getFacilityId()))
					&& textPattern.matcher(dock.getLocationId()).matches()
					&& textPattern.matcher(dock.getFacilityId()).matches()
					&& textPattern.matcher(dock.getDockStatus()).matches()
					&& textPattern.matcher(dock.getDockType()).matches()
					&& dockIdPattern.matcher(dock.getDockId()).matches()
					&& StringUtils.isNumeric(String.valueOf(dock.getDockTimeInterval()))) {
				flag=true;
				dock.setDockId(tempDock.getDockId().trim().toUpperCase());
				dock.setFacilityId(tempDock.getFacilityId().trim().toUpperCase());
				dock.setLocationId(tempDock.getLocationId().trim().toUpperCase());
				dock.setDockStatus(tempDock.getDockStatus().trim().toUpperCase());
				dock.setDockType(tempDock.getDockType().trim().toUpperCase());
				dock.setDockTimeInterval(tempDock.getDockTimeInterval());
			}
			return flag;
		}

		private void setCommonDockFields(Dock dockDto)
		{
			dockDto.setCreatedBy("User");
			dockDto.setLastUpdatedBy("User");
			dockDto.setDockOpenTime(CommonUtils.getTodayDockOpenTime().truncatedTo(ChronoUnit.MINUTES));
			dockDto.setDockCloseTime(CommonUtils.getTodayDockCloseTime().truncatedTo(ChronoUnit.MINUTES));
			dockDto.setDockExpectedReleaseTime(CommonUtils.getTodayDockCloseTime().truncatedTo(ChronoUnit.MINUTES));
			dockDto.setDockActualReleaseTime(CommonUtils.getTodayDockOpenTime().truncatedTo(ChronoUnit.MINUTES));
			dockDto.setCreatedDateTime(LocalDateTime.now().withNano(0).truncatedTo(ChronoUnit.MINUTES));
			dockDto.setLastUpdatedTimeStamp(LocalDateTime.now().withNano(0).truncatedTo(ChronoUnit.MINUTES));

		}
	}
